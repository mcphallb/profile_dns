# == Class: profile_bind
#
# Full description of class profile_bind here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { profile_bind:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2015 Your name here, unless otherwise noted.
#
class profile_bind {

  ## Hiera lookups
  $config_file         = hiera('profile_bind::config_file')
  $listen_on_addr      = hiera('profile_bind::listen_on_addr')
  $listen_on_addr_ipv6 = hiera('profile_bind::listen_on_addr_ipv6')
  $forwarders          = hiera('profile_bind::forwarders')
  $allow_query         = hiera('profile_bind::allow_query')
  $zones               = hiera('profile_bind::zones')

  ## Validate data
  validate_array($listen_on_addr)
  validate_array($listen_on_addr_ipv6)
  validate_array($forwarders)
  validate_array($allow_query)

  if !empty($zones) {
    validate_hash($zones)
  }

  ## Configure BIND
  include bind

  bind::server::conf{
    $config_file:
      listen_on_addr => $listen_on_addr,
      listen_on_addr_ipv6 => $listen_on_addr_ipv6,
      forwarders          => $forwarders,
      allow_query         => $allow_query,
      zones               => $zones,
  }

}
